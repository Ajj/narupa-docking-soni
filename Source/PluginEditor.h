/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include "../narupacppclient/NarupaClient.h"
#include "narupa/NarupaComponent.h"

//==============================================================================
/**
*/
class AudioClientAudioProcessorEditor  : public  AudioProcessorEditor
{
public:
    AudioClientAudioProcessorEditor (AudioClientAudioProcessor&);
    ~AudioClientAudioProcessorEditor();

    void paint(Graphics& g) override;

    void resized() override; 
    //==============================================================================

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    AudioClientAudioProcessor& processor;
    NarupaComponent narupaComponent;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (AudioClientAudioProcessorEditor)
};
