/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
AudioClientAudioProcessorEditor::AudioClientAudioProcessorEditor (AudioClientAudioProcessor& p)
    : AudioProcessorEditor (&p), 
      processor (p), narupaComponent(processor.getNarupaSynth())
{
    addAndMakeVisible(narupaComponent);
    setSize(350, 600);
}

AudioClientAudioProcessorEditor::~AudioClientAudioProcessorEditor()
{
}

//==============================================================================
void AudioClientAudioProcessorEditor::paint (Graphics& g){}

void AudioClientAudioProcessorEditor::resized()
{
    narupaComponent.setBounds(getLocalBounds());
}