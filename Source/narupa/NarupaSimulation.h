#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "../narupacppclient/NarupaClient.h"

#include <mutex>
#include <set>

class NarupaSimulation : private Thread
{
public:
    NarupaSimulation();
    ~NarupaSimulation();

    bool connect(const String& ipAddress);

    bool addNonBondedPair(int atomOne, int atomTwo) { return !commandService.addNonbondedPair(atomOne, atomTwo); };

    int getParticleCount();
    void getPositionsCopy(std::vector<float>& containerForPositionsCopy);
    void setTemperature(double newTemperature) { commandService.setTemperature(newTemperature); }
    void setFriction(double newFriction) { commandService.setFriction(newFriction); }
    void setTimestep(double newTimestep) { commandService.setTimestep(newTimestep); }
    void setPositionCallback(std::function<void(std::vector<float> positions)> callback)
    {
        trajectoryService.setPositionCallback(callback);
    }

    void setKineticEnergyCallback(std::function<void(float kinEnergy)> callback)
    {
        trajectoryService.setKinEnergyCallback(callback);
    }

    void setPotentialEnergyCallback(std::function<void(float kinEnergy)> callback)
    {
        trajectoryService.setPotEnergyCallback(callback);
    }

    void setNonBondedInteractionCallback(std::function<void(int particleFirstIndex, int particleSecondIndex, double value)> callback)
    {
        trajectoryService.setNonBondedInteractionCallback(callback);
    }

    /** scale = 100 is stable */
    void pluck(float scale, int durationMs);
    void pluck() { pluck(100, 500); };

    //class Listener
    //{
    //public:
    //    Listener() = default;
    //    virtual ~Listener() = default;
    //    virtual void simulationUpdate(const std::vector<float> newPositions) = 0;
    //};
    //void addListener(NarupaSimulation::Listener* listenerToAdd);
    //void removeListener(NarupaSimulation::Listener* listenerToRemove);

private:
    //thread for event dispatching
    void run() override;

    struct UniqueIdGenerator
    {
        int64 get()
        {
            static int64 uniqueId = 0;
            return uniqueId++;
        }
    };

    UniqueIdGenerator idGenerator;

    struct InteractionEvent
    {
        InteractionEvent(int64 t, const ImdService::Interaction& i, bool es)
            : time(t), interaction(i), endStream(es) {}
        int64 time{ 0 };
        ImdService::Interaction interaction;
        bool endStream{ false };
    };

    std::mutex eventListLock;
    std::vector<InteractionEvent> eventList;

    void addEvent(int64 timeForEvent, const ImdService::Interaction& eventToAdd)
    {
        addEvent(timeForEvent, eventToAdd, false);
    }

    void addEvent(int64 timeForEvent, const ImdService::Interaction& eventToAdd, bool isLastStreamEvent)
    {
        std::lock_guard<std::mutex> lg(eventListLock);
        //insert in time order
        auto it = std::lower_bound(eventList.begin(), eventList.end(), timeForEvent, [](const InteractionEvent& lhs, int64 rhs) -> bool
            { return lhs.time < rhs; });
        eventList.emplace(it, timeForEvent, eventToAdd, isLastStreamEvent); // insert before iterator it
        notify(); //wake up the event dispatcher
    }

    TrajectoryService trajectoryService;
    CommandService commandService;
    ImdService imdService;
    std::mutex positionsLock;
    std::vector<float> positions;

    std::mutex listenerLock;
    ListenerList<NarupaSimulation::Listener> listeners;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(NarupaSimulation)
};