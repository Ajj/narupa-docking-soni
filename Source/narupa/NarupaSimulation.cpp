#include "NarupaSimulation.h"

NarupaSimulation::NarupaSimulation() : Thread("InteractionEventThread")
{
    startThread();
}

NarupaSimulation::~NarupaSimulation()
{
    stopThread(1000);
}

bool NarupaSimulation::connect(const String& ipAddress)
{
     auto ret = trajectoryService.connect(ipAddress.toStdString()) && commandService.connect(ipAddress.toStdString());

     return ret;
}

int NarupaSimulation::getParticleCount()
{
    std::lock_guard<std::mutex> lg(positionsLock);
    return positions.size() / 3;
}

void NarupaSimulation::pluck(float scale, int durationMs)
{
    int64 currentTime = Time::currentTimeMillis();
    const std::string id = std::to_string(idGenerator.get());
    ImdService::Interaction interaction(id, { 0.f, 0.f, 0.f }, 468, scale, ImdService::Interaction::ForceType::Spring);
    addEvent(currentTime, interaction);
    addEvent(currentTime + durationMs, interaction, true);
}

void NarupaSimulation::getPositionsCopy(std::vector<float>& containerForPositionsCopy)
{
    std::lock_guard<std::mutex> lg(positionsLock);
    containerForPositionsCopy = positions;
}

//void NarupaSimulation::addListener(NarupaSimulation::Listener* listenerToAdd)
//{
//    std::lock_guard<std::mutex> lg(listenerLock);
//    listeners.add(listenerToAdd);
//}
//
//void NarupaSimulation::removeListener(NarupaSimulation::Listener* listenerToRemove)
//{
//    std::lock_guard<std::mutex> lg(listenerLock);
//    listeners.remove(listenerToRemove);
//}

void NarupaSimulation::run()
{
    while (!threadShouldExit())
    {
        int waitTime = 100;
        {
            std::lock_guard<std::mutex> lg(eventListLock);

            for (auto iter = eventList.begin(); iter != eventList.end(); )
            {
                auto eventTime = iter->time;
                auto currentTime = Time::currentTimeMillis();

                if (currentTime >= eventTime)
                {
                    if (iter->endStream)
                        imdService.endInteraction(iter->interaction.id);
                    else
                        imdService.beginOrUpdateInteration(iter->interaction);

                    iter = eventList.erase(iter);
                }
                else
                {
                    waitTime = eventTime - currentTime;
                    ++iter;
                    break; //leave subsequent events
                }
            }
        }
        wait(waitTime);
    }
}
