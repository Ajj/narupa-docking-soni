#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "NarupaSynthesiser.h"

class NarupaComponent : public Component, public Timer
{
public: 
    NarupaComponent(NarupaSynthesiser& synth_);
    ~NarupaComponent();

    void paint(Graphics&) override;
    void resized() override;

private:
    NarupaSynthesiser& synth;

    void timerCallback() override;

    Label ipLabel{ "", "localhost" };
    TextButton connectButton{ "Connect" };
    Label kineteicEnergyLabel;
    TextEditor editor;

    //Slider keSlider;
};


