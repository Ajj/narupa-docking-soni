#include "NarupaComponent.h"

NarupaComponent::NarupaComponent(NarupaSynthesiser& synth_) : synth(synth_)
{
    ipLabel.setEditable(true);
    addAndMakeVisible(ipLabel);

    connectButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    connectButton.onClick = ([this]()
    {
        synth.connectToServer(ipLabel.getText());
    });
    addAndMakeVisible(connectButton);

    kineteicEnergyLabel.setText("", dontSendNotification);
    addAndMakeVisible(kineteicEnergyLabel);

    editor.setMultiLine(true);
    addAndMakeVisible(editor);

    setSize(400, 300);

    startTimerHz(1);
}

NarupaComponent::~NarupaComponent()
{
}

void NarupaComponent::paint(Graphics&)
{
}

void NarupaComponent::resized()
{
    Rectangle<int> r = getLocalBounds();
    auto temp = r.removeFromTop(20);
    ipLabel.setBounds(temp.removeFromLeft(r.getWidth() - 100));
    connectButton.setBounds(temp);
    kineteicEnergyLabel.setBounds(r.removeFromTop(20));
    editor.setBounds(r);
}

void NarupaComponent::timerCallback()
{
    kineteicEnergyLabel.getTextValue() = "Kinetic Energy: " + String(synth.getKineticEnergy());
    std::map<const std::string, double> nonBondedInteractions;
    synth.getNonBondedInteractions(nonBondedInteractions);
    editor.clear();
    editor.insertTextAtCaret("Non-Bonded Interactions\n");
    for (auto& e : nonBondedInteractions)
        editor.insertTextAtCaret(e.first + " " + std::to_string(e.second) + "\n");
}
