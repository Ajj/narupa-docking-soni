#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "../narupacppclient/NarupaClient.h"
#include "NarupaSimulation.h"
#include "../utility/Window.h"

#include <mutex>
#include <set>

class OnePoleFilter
{
public:
    OnePoleFilter(float coeff_)
    {
        init(coeff_);
    }
    void setId(int id_) { id = id_; }
    int getId() { return id; }
    void setTargetValue(float newValue)
    {
        previousValue = newValue;
    }
    void init(float coeff)
    {
        a = coeff;
        b = 1.0f - coeff;
    }
    float getNextSample(float input)
    {
        return previousValue = (a * input) + (b * previousValue);
    }
private:
    float a{ 0.0f };
    float b{ 0.0f };
    float previousValue{ 0.0f };
    int id{ 0 };
};

class NarupaSynthesiser : public HighResolutionTimer
{
public:
    NarupaSynthesiser();
    ~NarupaSynthesiser();
    
    void getNonBondedInteractions(std::map<const std::string, double>& destination) const
    {
        std::lock_guard<std::mutex> lg(lock);
        destination = nonBondedInteractions;
    }

    bool connectToServer(const String& ipAddress);

    void initialiseNonBondedPairs();
    void getDistanceActiveSiteLigand();
    void calculateRMSD(std::vector<int> setA, std::vector<int> setB);
    //==============================================================================
    void loadAtomSelections(File file);
    void loadInteractionGroups(File file);
    void hiResTimerCallback() override;

    float getKineticEnergy() const { return keNormal.load(); }
    float getPotentialEnergy() const { return peNormal.load(); }
   
    struct InteractionGroup
    {
        std::vector<int> electrophiles;
        std::vector<int> nucleophiles;
        int numInteractions{ 0 };
        float totalNonBondedEnergy{ 0 };
    };

private:
    NarupaSimulation narupaSimulation;
    bool logging = true;

    std::map<const std::string, double> nonBondedInteractions;
    std::vector<InteractionGroup> interactionGroups;

    std::vector<double> nonBondedEnergies;

    std::unique_ptr<FileLogger> logger;

    // Atom selection Indices
    std::vector<int> ligandIndices;
    std::vector<int> activeStideIndices;
    
    std::atomic<float> kineticEnergy{ 0.f };
    std::atomic<float> kineticEnergyRaw{ 0.f };
    mutable std::mutex lock;
    
    float RMSD;
    std::mutex positionsLock;
    std::vector<float> initialPositions;
    std::vector<float> positions;

    OnePoleFilter filter;

    std::atomic<float> potentialEnergyRaw = 0;
    std::atomic<float> peNormal = 0;
    // for analysis of the window function
    const static int numKELogs = 2;
    std::unique_ptr<Window> window;
    std::atomic<float> keNormal;

    std::atomic<float> minLog;
    std::atomic<float> maxLog;

    int timerCounter  = 0;
};


















