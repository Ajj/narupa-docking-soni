#include "NarupaSynthesiser.h"
#include "..\..\Source\narupa\NarupaSynthesiser.h"

NarupaSynthesiser::NarupaSynthesiser() : filter(0.05)
{
    // Get the interaction groups from file
    File file = File::getCurrentWorkingDirectory().getChildFile("../../Simulations/Interaction_Groups/Neuraminidase_InteractionGroups.xml");
    loadInteractionGroups(file);

    // Get the atom selection groups from file
    File selectionFile = File::getCurrentWorkingDirectory().getChildFile("../../Visualisations/neuraminidase_complex.json");
    loadAtomSelections(selectionFile);

    // Set up the logger to store non-bonded interaction values
    //File logFile = File::getCurrentWorkingDirectory().getChildFile("../../Analysis/nbie_log.csv");
    // Set up the logger to store kinetic energy values
    File logFile = File::getCurrentWorkingDirectory().getChildFile("../../Analysis/ke_log.csv");

    logger.reset(new FileLogger(logFile, "non-bonded interaction energies for a single pair"));
    
    startTimer(10);

    window.reset(new Window(20, 2, 20));
   
    //narupaSimulation.setPotentialEnergyCallback([&](float value)
    //    {
    //        potentialEnergyRaw = value;

    //        const auto windowVal = window.get()->getNormalisedValue(value);
    //        const auto clamp = std::clamp(windowVal, 0.f, 1.f);

    //        peNormal = std::powf(clamp, 3);
    //    });

    narupaSimulation.setKineticEnergyCallback ([&](float value)
    {
            kineticEnergyRaw = value;

            const auto windowVal = window.get()->getNormalisedValue(value);
            const auto filtered = filter.getNextSample(windowVal);
            const auto clamp = std::clamp(windowVal, 0.f, 1.f);

            keNormal = std::powf(clamp, 3);

            //const auto minL = window.get()->getCurrentMin();
            //const auto maxL = window.get()->getCurrentMax();
            //const auto mean = window.get()->getCurrentMean();
            //const auto sd = window.get()->getCurrentSD();

                    if (kineticEnergyRaw.load() != 0 && !isnan(kineticEnergyRaw.load()) && keNormal.load() != 0)
                    {
                            String m = "KE nor / " + (String)keNormal.load();
                            logger.get()->logMessage(m);

                            String mr = "KE raw / " + (String)kineticEnergyRaw.load();
                            logger.get()->logMessage(mr);

                            /*String minM = "Min / " + (String)minL;
                            logger.get()->logMessage(minM);

                            String maxM = "Max / " + (String)maxL;
                            logger.get()->logMessage(maxM);

                            String meanM = "Mean / " + (String)mean;
                            logger.get()->logMessage(meanM);

                            String sdM = "SD / " + (String)sd;
                            logger.get()->logMessage(sdM);*/
                        timerCounter++;
                    }
    });

    narupaSimulation.setNonBondedInteractionCallback([this](int particleFirstIndex, int particleSecondIndex, double value)
    {
        const auto key (std::to_string(particleFirstIndex) + ":" + std::to_string(particleSecondIndex));
        std::lock_guard<std::mutex> lg (lock);
        nonBondedInteractions[key] = value;
    });

    narupaSimulation.setPositionCallback([&](std::vector<float> newPositions)
    {
        std::lock_guard<std::mutex> lg(positionsLock);
        positions.resize(newPositions.size());
        positions = newPositions;
        getDistanceActiveSiteLigand();
        //DBG(RMSD);
    });
}

NarupaSynthesiser::~NarupaSynthesiser()
{
}

bool NarupaSynthesiser::connectToServer(const String& ipAddress)
{
    bool ret = narupaSimulation.connect(ipAddress);
    initialiseNonBondedPairs();
    
    return ret;
}

void NarupaSynthesiser::initialiseNonBondedPairs()
{
    for (auto group : interactionGroups)
    {
        for (auto atomOne : group.nucleophiles)
        {
            for (auto atomTwo : group.electrophiles)
            {
                DBG("Adding non-bonded pair for " << atomOne << " and " << atomTwo);
                if (!narupaSimulation.addNonBondedPair(atomOne, atomTwo))
                {
                    DBG("Non Bonded Command Failed for atom pair " << atomOne << " and " << atomTwo);
                }
            }
        }
    }
}

void NarupaSynthesiser::getDistanceActiveSiteLigand()
{
    int count = 0;

    std::array<float, 3>  averageligandPosition;
    for (auto index : ligandIndices)
    {
        auto offset = index * 3;
        averageligandPosition[0] += positions[offset];
        averageligandPosition[1] += positions[offset + 1L];
        averageligandPosition[2] += positions[offset + 2L];
    }
    averageligandPosition[0] /= ligandIndices.size();
    averageligandPosition[1] /= ligandIndices.size();
    averageligandPosition[2] /= ligandIndices.size();


    std::array<float, 3>  averageActiveSitePosition;
    for (auto index : activeStideIndices)
    {
        auto offset = index * 3;

        averageActiveSitePosition[0] += positions[offset];
        averageActiveSitePosition[1] += positions[offset + 1L];
        averageActiveSitePosition[2] += positions[offset + 2L];
    }
    averageActiveSitePosition[0] /= activeStideIndices.size();
    averageActiveSitePosition[1] /= activeStideIndices.size();
    averageActiveSitePosition[2] /= activeStideIndices.size();

    auto x = (averageActiveSitePosition[0] - averageligandPosition[0]);
    auto y = (averageActiveSitePosition[1] - averageligandPosition[1]);
    auto z = (averageActiveSitePosition[2] - averageligandPosition[2]);

    RMSD = sqrtf((x * x) + (y * y) + (z * z));
}

void NarupaSynthesiser::calculateRMSD(std::vector<int> setA, std::vector<int> setB)
{
    float sum = 0.0;

    jassert(setA.size() == setB.size());

    for (auto i : setA)
    {
        auto iOffset = i * 3;
        for (auto j : setB)
        {
            auto jOffset = j * 3;
            for (int q = 0; q < 3; q++)
            {
                sum += std::powf(positions[iOffset + q] - positions[jOffset + q], 2);
            }
        }
    }
    RMSD = std::sqrtf((1.0f / setA.size()) * sum);
}

void NarupaSynthesiser::loadAtomSelections(File file)
{
    var selectionData;

    if (selectionData = JSON::parse(file))
    {
        for (int i = 0; i < selectionData.size(); i++)
        {
            String name = selectionData[i]["Name"];

            if (name == "Ligand")
            {
                auto ligandSelection = selectionData[i]["SelectedAtoms"];

                for (int j = 0; j < ligandSelection.size(); j++)
                {
                    ligandIndices.push_back(ligandSelection[j]);
                }
            }
            // The selections for the active site have many different names, so they are defined based on what they are not
            if (name != "Ligand" && name != "Surface" && name != "base" && name != "Backbone")
            {
                auto activeSiteSelection = selectionData[i]["SelectedAtoms"];

                for (int j = 0; j < activeSiteSelection.size(); j++)
                {
                    activeStideIndices.push_back(activeSiteSelection[j]);
                }
            }
        }
    }
}

void NarupaSynthesiser::loadInteractionGroups(File file)
{
    XmlDocument interactionGroupsXml(file);

    // load the data into some member variables
    if (auto mainElement = interactionGroupsXml.getDocumentElement())
    {
        forEachXmlChildElement(*mainElement, group)
        {
            XmlElement* nucleophiles = group->getChildByName("Nucleophiles");
            XmlElement* electrophiles = group->getChildByName("Electrophiles");

            InteractionGroup* newGroup = new InteractionGroup;
            forEachXmlChildElement(*nucleophiles, atomOne)
            {
                newGroup->nucleophiles.push_back(atomOne->getIntAttribute("id"));
            }
            forEachXmlChildElement(*electrophiles, atomTwo)
            {
                newGroup->electrophiles.push_back(atomTwo->getIntAttribute("id"));
            }
            newGroup->numInteractions = newGroup->electrophiles.size() * newGroup->nucleophiles.size();
            interactionGroups.push_back(*newGroup);
        }
    }
    else
    {
        DBG("Error loading XML");
    }
}

void NarupaSynthesiser::hiResTimerCallback()
{
    if (logging)
    {
        // Log nonbondede energy for whole group of atom pairs
        //MessageManager::callAsync([&]
        //    {
        //        int groupNumber = 0;
        //        for (auto group : interactionGroups)
        //        {
        //            group.totalNonBondedEnergy = 0;
        //            for (auto atomOne : group.electrophiles)
        //            {
        //                for (auto atomTwo : group.nucleophiles)
        //                {
        //                    const auto key(std::to_string(atomTwo) + ":" + std::to_string(atomOne));
        //                    auto value = nonBondedInteractions[key];

        //                    // log the energy for each atom pair
        //                    /*if (value != 0 && !isnan(value))
        //                    {
        //                        String message = (String)atomOne + "-" + (String)atomTwo + " / " + (String)value;
        //                        logger.get()->logMessage(message);
        //                    }*/
        //                    group.totalNonBondedEnergy += value;
        //                }
        //            }

        //            auto average = group.totalNonBondedEnergy / (float)group.numInteractions;
        //            if (average != 0 && !isnan(average))
        //            {
        //                String m = "G" + (String)groupNumber + " / " + (String)average;
        //                logger.get()->logMessage(m);
        //            }
        //            groupNumber++;
        //        }
        //        String dmessage = "RMSD / " + (String)RMSD;
        //        logger.get()->logMessage(dmessage);
        //    });

        // Log kinetic energy for whole system

            const auto minL = window.get()->getCurrentMin();
            const auto maxL = window.get()->getCurrentMax();
            MessageManager::callAsync([&]
                {
                    if (kineticEnergyRaw.load() != 0 && !isnan(kineticEnergyRaw.load()) && keNormal.load() != 0)
                    {
                        if (timerCounter < 50)
                        {
                            String m = "KE nor / " + (String)keNormal.load();
                            logger.get()->logMessage(m);

                            String mr = "KE raw / " + (String)kineticEnergyRaw.load();
                            logger.get()->logMessage(mr);
                        }
                        timerCounter++;
                    }
                });
    
    }
}

//void NarupaSynthesiser::simulationUpdate(const std::vector<float> newPositions)
//{
//    DBG(positions.size());
//    int groupNumber = 0;
//    for (auto group : interactionGroups)
//    {
//        group.totalNonBondedEnergy = 0;
//        for (auto atomOne : group.electrophiles)
//        {
//            for (auto atomTwo : group.nucleophiles)
//            {
//                const auto key(std::to_string(atomTwo) + ":" + std::to_string(atomOne));
//                auto value = nonBondedInteractions[key];
//                group.totalNonBondedEnergy += value;
//            }
//        }
//        auto average = group.totalNonBondedEnergy / (float)group.numInteractions;
//        groupNumber++;
//    }
//}









