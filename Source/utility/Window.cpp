#include "Window.h"
#include <numeric>

Window::Window(int size, float numSD_, int numBuckets_) : numSD(numSD_), numBuckets(numBuckets_)
{
    //buffer.resize(size);
    bufferSize = size;
}

Window::~Window()
{

}

// Update is called once per frame
float Window::getNormalisedValue(float nextValue)
{
    if (isnan<float>(nextValue) || nextValue == 0)
        return 0.0;

    std::vector<float>::iterator it = buffer.begin() + bufferWritePos;
    buffer.insert(it, nextValue);

    bufferWritePos++;

    auto normalisedValue = 0.0;

    if (bufferWritePos > bufferSize - 1)
    {
        bufferWritePos = 0;
        distribution = histogram.getBuckets(buffer, numBuckets);
        mean = 0.0f;
        standardDeviation = 0.0f;

        auto size = (float)buffer.size();

        if (size > 0)
        {
            auto sum = std::accumulate(buffer.begin(), buffer.end(), 0.f);
            mean = sum / size;
       
            auto sq_sum = std::inner_product(buffer.begin(), buffer.end(), buffer.begin(), 0.f);
            
            standardDeviation = std::sqrtf(sq_sum / size - mean * mean);
        }

        min = (mean - (numSD * standardDeviation)) * 0.4;
        max = (mean + (numSD * standardDeviation)) * 1.6;

        go = true;
    }

    if (go)
        normalisedValue = (nextValue - min) / (max - min);

    if (normalisedValue > 1.0)
        normalisedValue = 1.0;
    else if (normalisedValue < 0.0)
        normalisedValue = 0.0;

    return normalisedValue;
}
