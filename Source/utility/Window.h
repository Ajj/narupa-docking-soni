#pragma once
#include <vector>
#include "Histogram.h" 

#include "../JuceLibraryCode/JuceHeader.h"

class Window
{
public:
    Window(int size, float numSD_, int numBuckets_);
    ~Window();

    float getNormalisedValue(float nextValue);
    float getCurrentMin() { return min; }
    float getCurrentMax() { return max; }
    float getCurrentSD() { return standardDeviation; }
    float getCurrentMean() { return mean; }

private:
    Window() {}
    Histogram histogram;
    std::vector<float> buffer;
    bool go = false;
    int bufferSize = 50;
    int bufferWritePos = 0;
    float standardDeviation = 0.f;
    float mean = 0.f;
    float min = 0.f;
    float max = 0.f;

    float numSD = 2;
    int numBuckets = 20;
    std::vector<float> distribution;
};
