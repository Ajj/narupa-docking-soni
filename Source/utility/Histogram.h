#pragma once
#include <vector>

class Histogram
{
public:
    Histogram();
    ~Histogram();
    std::vector<float> getBuckets (const std::vector<float>& source, int totalBuckets);
private:

};

