#!/usr/bin/env python

"""
Convert an input from NarupaXR to an input for Narupa 2.
"""

import argparse
from xml.dom.minidom import getDOMImplementation, parseString, Element

import numpy as np

from narupa.openmm.serializer import _get_node_and_raise_if_more_than_one, serialize_simulation
from narupa.openmm.potentials import restrain_particles, DEFAULT_RESTRAINT_FORCE_CONSTANT
from simtk.openmm import app, XmlSerializer
from simtk import unit, openmm


def get_atom_masses(topology):
    return [atom.element.mass for atom in topology.atoms()]

def build_system(document, topology):
    original_node = _get_node_and_raise_if_more_than_one(document, 'OpenMMForceField')

    build_node = document.createElement('System')
    build_node.setAttribute('openmmVersion', '7.3.1')
    build_node.setAttribute('type', 'System')
    build_node.setAttribute('version', '1')

    box_vectors = get_box_vectors(document)
    box_node = document.createElement('PeriodicBoxVectors')
    for vector, name in zip(box_vectors, 'ABC'):
        node = document.createElement(name)
        for value, dimension in zip(vector, 'xyz'):
            node.setAttribute(dimension, str(value.value_in_unit(unit.nanometer)))
        box_node.appendChild(node)
    build_node.appendChild(box_node)

    particles_node = document.createElement('Particles')
    for mass in get_atom_masses(topology):
        particle_node = document.createElement('Particle')
        particle_node.setAttribute('mass', str(mass))
        particles_node.appendChild(particle_node)
    build_node.appendChild(particles_node)

    constraints_node = _get_node_and_raise_if_more_than_one(document, 'Constraints')
    build_node.appendChild(constraints_node)

    kept_potentials = (
        'CMMotionRemover',
        'GBSAOBCForce',
        'HarmonicAngleForce',
        'HarmonicBondForce',
        'NonbondedForce',
        'PeriodicTorsionForce',
    )
    forces_node = document.createElement('Forces')
    all_potential_nodes = original_node.getElementsByTagName('Force')
    for potential_node in all_potential_nodes:
        potential_name = potential_node.getAttribute('type')
        if potential_name in kept_potentials:
            forces_node.appendChild(potential_node)
    build_node.appendChild(forces_node)

    content = build_node.toprettyxml()
    system = XmlSerializer.deserialize(content)
    return system


def get_thermostat(document):
    node = _get_node_and_raise_if_more_than_one(document, 'Thermostat')
    temperature = float(node.getAttribute('EquilibriumTemperature')) * unit.kelvin
    thermostat_type = node.getAttribute('Type')
    if thermostat_type == 'AndersenThermostat':
        collision_frequency = node.getAttribute('CollisionFrequency')
        collision_frequency = float(collision_frequency) * ( 1 / unit.picosecond)
        force = openmm.AndersenThermostat(temperature, collision_frequency)
        return force
    raise NotImplementedError(f'Unknown thermostat type "{thermostat_type}".')


def get_integrator(document):
    node = _get_node_and_raise_if_more_than_one(document, 'Integrator')
    timestep = float(node.getAttribute('TimeStep')) * unit.picosecond
    integrator_type = node.getAttribute('Type')
    if integrator_type == 'Verlet':
        integrator = openmm.VerletIntegrator(timestep)
        return integrator
    raise NotImplementedError(f'Unknown integrator "{integrator_type}".')


def get_box_vectors(document):
    node = _get_node_and_raise_if_more_than_one(document, 'SimulationBoundary')
    box_string = node.getAttribute('SimulationBox')
    box_length = [float(x) for x in box_string.split(',')]
    box_vectors = [
        [box_length[0], 0, 0] * unit.nanometer,
        [0, box_length[1], 0] * unit.nanometer,
        [0, 0, box_length[2]] * unit.nanometer,
    ]
    return box_vectors


def get_restraints(document, positions):
    node = _get_node_and_raise_if_more_than_one(document, 'ParticleRestraints')
    indices = []
    force_constant = None
    for restraint in node.childNodes:
        if isinstance(restraint, Element):
            indices.append(int(restraint.getAttribute('AtomIndex')))
            current_force_constant = restraint.getAttribute('K')
            dimensions = restraint.getAttribute('RestraintDimensions')
            if force_constant is None:
                force_constant = current_force_constant
            elif force_constant != current_force_constant:
                raise NotImplementedError('All force constant must ne the same.')
            if dimensions != 'XYZ':
                raise NotImplementedError('Only 3D position restraints are implemented.')
    if force_constant:
        force_constant = float(force_constant) * unit.kilojoule_per_mole / unit.nanometer ** 2
    else:
        force_constant = DEFAULT_RESTRAINT_FORCE_CONSTANT
    force = restrain_particles(positions, indices, force_constant)
    return force


def center_pdb(pdb, box_vectors):
    # Narupa XR centers the system on 0, 0, 0. Here we want the system
    # centered at the middle of the box.
    positions = np.array(pdb.positions.value_in_unit(unit.nanometer))
    box_lengths = np.array([
        box_vectors[0][0].value_in_unit(unit.nanometer),
        box_vectors[1][1].value_in_unit(unit.nanometer),
        box_vectors[2][2].value_in_unit(unit.nanometer),
    ])
    bounding_box = positions.max(axis=0) - positions.min(axis=0)
    if np.any(bounding_box > box_lengths):
        print("WARNING! The bounding box of the system is larger than the box!")
        print(f"Bounding box: {bounding_box}")
        print(f"Box:          {box_lengths}")
    center = box_lengths / 2
    positions += center
    pdb.positions = positions * unit.nanometer


def build_simulation(pdb_path, xml_path):
    pdb = app.PDBFile(pdb_path)
    with open(xml_path) as infile:
        document = parseString(infile.read())
    
    box_vectors = get_box_vectors(document)
    center_pdb(pdb, box_vectors)

    system = build_system(document, pdb.topology)
    thermostat = get_thermostat(document)
    restraints = get_restraints(document, pdb.positions)
    system.addForce(thermostat)
    system.addForce(restraints)

    integrator = get_integrator(document)

    simulation = app.Simulation(
        topology=pdb.topology,
        system=system,
        integrator=integrator,
    )
    simulation.context.setPositions(pdb.positions)
    simulation.context.setPeriodicBoxVectors(*box_vectors)

    return simulation


def write_xml_input(pdb_path, xml_in_path, xml_out_path):
    simulation = build_simulation(pdb_path, xml_in_path)
    xml_content = serialize_simulation(simulation)
    with open(xml_out_path, 'w') as outfile:
        outfile.write(xml_content)


def get_user_input():
    parser = argparse.ArgumentParser()
    parser.add_argument('--pdb')
    parser.add_argument('--in', dest='in_path')
    parser.add_argument('--out', dest='out_path')

    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = get_user_input()
    write_xml_input(
        args.pdb, args.in_path, args.out_path,
    )